---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2020/2021
titre: PMF-Vision - Systeme de projection pour table de montage intelligente
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Omar Abou Khaled
  - Maurizio Caon 
  - Simon Ruffieux 
mots-clé: [Augmented Reality, Projection, Assembly Workshop]
langue: [F,E]
confidentialité: oui
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.6\textwidth]{img/PMF-Vision_1.png}
\end{center}
```

## Contexte

L'entreprise PMF-System SA basée à Marly produit des meubles spécialement pensés pour les entreprises. Elle produit notamment des tables de montage (Assembly Workspace) pour diverses PME locales et cherche à rendre ces tables intelligentes. La table de montage intelligente sera composée d'une caméra, d'une lampe et d'un projecteur (tous installés au-dessus de la table et pointant vers le bas (top-view)).
L'utilisation de la caméra  permet de suivre les actions effectuées par l'opérateur (suivi de la position des mains, prendre une pièce, déposer une pièce) afin de s'assurer que toutes les étapes de l'assemblage sont réalisées dans le bon ordre. Un projecteur sera utilisé pour fournir des informations à l'utilisateur en réalité augmentée directement sur l'espace de travail et potentiellement pour mettre en valeur certaines parties du montage en cours de réalisation.

A terme, ce système a pour but de simplifier le contrôle qualité des produits réalisés et d'aider les opérateurs à apprendre de nouveaux scénarios de montage. Ce système sera déployé dans différentrs ateliers-protégés partenaires.

Dans le cadre de ce projet, l'étudiant devra analyser les systèmes permettant de projeter de l’information directement sur l’espace de travail en analysant le matériel (les projecteurs) répondant aux contraintes spécifiques du projet. Une fois l’analyse matérielle réalisée, il développera une petite application permettant de démontrer la projection d’information à différents endroits de l’espace de travail. Une table d'assemblage, fournie par PMF-System, est disponibles dans locaux et pourra être utilisée pour tester le prototype développé en conditions réélles. 

*De plus, ce projet pourrait donner lieu à une collaboration inédite et originale avec des étudiants de la HEG de Fribourg (3ème année Bachelor – orientation Digital Business) qui réaliseraient une étude de marché autour de ce projet afin de cerner les potentialités économiques d’une telle table pour d’autres domaines que les ateliers-protégés.  Ils devront également créer et concevoir (design) un nouveau modèle d’affaires et de services autour de cette table.
Cette expérience interdisciplinaire, qui a pour but de faire émerger des solutions innovantes sur le plan technologique et économique, pourrait donner lieu à des rencontres entre les deux équipes ainsi que l’entreprise et les superviseurs lors desquelles, chacun devra présenter son travail afin de réaliser des réflexion communes *


## Objectifs

- Analyse des systèmes de projection disponibles sur le marché
- Analyse des systèmes commerciaux similaires
- Sélection d'un projecteur adapté aux contraintes du projet
- Réalisation d'une application de démonstration de projection d'information
- Evaluation du système

## Contraintes

- Python
