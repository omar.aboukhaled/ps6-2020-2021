---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2020/2021
titre: Analyse et traitement des vidéos
filières:
  - Informatique
  - Télécommunications
  - ISC
nombre d'étudiants: 1

professeurs co-superviseurs:
  - Omar Abou Khaled
  - Karl Daher
  - Didier Crausaz
mots-clés: [noise reduction, audio to text, video analysis]
langue: [F,E]
confidentialité: non
suite: non
---

## Description/Contexte

Colearnis est une platforme de partage du savoir faire. Des vidéos sont téléchargés par les collaborateurs sur cette platforme. Ces vidéos sont des fois bruyantes et la narration du réalisateur est imcompréhensible. Une analyse des vidéos et un traitement précis doit être effectuer. Ce projet consiste d'une recherche sur des APIs et des application existantes sur:
- Réduction du bruit dans les videos.  
- Implémentation du speech to text

Le but c'est d'avoir des videos qui sont assez propres pour que les nouveaux collaborateurs puissent l'utiliser comme vidéo d'apprentissage. 
Les vidéos sont déjà enregistrées, les tests seront effectués directement dessus.

## Objectifs/Tâches
 
- Recherche des technologies et API 
- Tester les différentes technogies existantes
- Analyse des résultats des vidéos
- Validation des vidéos propores
