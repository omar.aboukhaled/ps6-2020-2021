---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2020/2021
titre: La réalité virtuelle pour faciliter la participation sociale des jeunes ayant une incapacité intellectuelle 
filières:
  - Informatique
  - Télécommunications
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Omar Abou Khaled
  - Francesco Carrino
  - Robin Cherix
mots-clés: [Virtual Reality, Unity, incapacité intellectuelle]
langue: [F,E]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/vrhands.png}
\end{center}
```

## Description/Contexte

Les adolescents et jeunes adultes ayant une incapacité intellectuelle peuvent se retrouver en situation(s) de handicap dans la réalisation de certaines habitudes de vie (p. ex. traverser la route, prendre le bus, gérer des conversations avec des inconnus, etc.).
Les technologies de casques de réalité virtuelle permettent aujourd’hui de fournir un cadre contrôlé et sans risque pour y recréer les mises en scène nécessaires à l’apprentissage de ces personnes. Les exercices avec la réalité virtuelle permettront aux jeunes d’acquérir de nouvelles compétences qui pourront être ensuite exercées en situation réelle sous le contrôle de l’éducateur social.
Un premier prototype est aujourd’hui fonctionnel et permet de simuler une situation où l’utilisateur doit traverser la route et/ou prendre en bus.
Ce projet de semestre vise à étudier la faisabilité, les avantages et les limites d’introduire dans la simulation une représentation virtuelle des mains de l’utilisateur (voir image). Cela sera fait en utilisant des technologies comme l’Oculus Quest 2. Ceci sera fait en deux étapes :
- Développement d’un scénario de test « stand-alone », à définir au début du projet
- Intégration de l’approche développée dans les scénarios existants



## Objectifs

- Objectifs principaux :
	+ Réalisation d’un prototype pour étudier la faisabilité, les avantages et les limites d’introduire dans la simulation une représentation virtuelle des mains de l’utilisateur
	+ Intégration de l’approche développée dans les scénarios existants
- Objectif secondaire :
	+ Intégration de l’interaction gestuelle dans les scénarios existants 


## Tâches

- Analyse
    + Prise en main du projet existant
    + Prise en main de technologies (Unity, VR, Oculus)
    + Analyse des besoins
- Conception
    + Définition du scénario de test
    + Définition de l’architecture du système
- Développement et tests
    + Selon l’avancement du projet et les intérêts de l’étudiant ça sera possible de mener de tests sur le terrain en partenariat avec l’Haute École de Travail Social 


