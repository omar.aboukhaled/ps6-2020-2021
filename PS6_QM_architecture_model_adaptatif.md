---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2020/2021
titre: Intégration d'un modèle adaptatif pour véhicule autonome
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Omar Abou Khaled
  - Marine Capallera
  - Quentin Meteier

mots-clés: [Python, JSON, Websocket, architecture logicielle, conduite autonome, interaction homme-véhicule]
langue: [F,E]
confidentialité: non
suite: non
---

## Description/Contexte

Le projet AdVitam, mené par l’Institut HumanTech en collaboration avec l’Université de Fribourg et l’He-Arc, vise à comprendre quelles sont les situations dangereuses pour la conduite semi-autonome d’un véhicule et ainsi développer de nouvelles interfaces homme-voiture afin que le conducteur puisse mieux gérer ces situations. Le but global du projet est de créer un modèle permettant d'adapter en temps réel l'intéraction entre le conducteur et le véhicule, en fonction de son état physiologique (stressé, fatigué, ...) et de l'état de la situation de conduite (mauvais temps, potentiels dangers, ...). Les différentes interfaces qui peuvent être utilisées pour maintenir la conscience du conducteur sont : des vibrations dans le siège (Gilet BHaptics + Unity), une application mobile (Android) et des lumières ambiantes (Arduino).

Différents modules du modèle ont déjà été implémentés : plusieurs scénarios de conduite (Unity) avec envoi de trames (JSON/Websocket), des modèles de Machine Learning permettant de prédire l'état du conducteur (Python/Scikit Learn), un modèle de règles permettant de chosir l'interface à utiliser dans la voiture (Python/Durable rules) et un autre modèle de machine learning permettant de choisir la meilleure façon de prévenir le conducteur qu'une reprise de contrôle est nécessaire (Python).

Le but de ce projet de semestre est d'intégrer tous ces différents modules afin de les faire communiquer entre eux. L'objectif primaire est de récupérer les trames provenant du simulateur ainsi que celles de la prédiction de l'état du conducteur, et les mettre à diposition des différents modules pernettant de choisir les interfaces à utiliser dans la voiture, et enfin renvoyer l'information au simulateur pour activer la bonne interface (vibration, lumières ou application mobile). 


## Objectifs

Durant ce projet de semestre, l’étudiant devra accomplir les tâches suivantes :

1. Prendre connaissance des différentes parties du modèle déjà implémentées
2. Concevoir  et proposer des architectures permettant de résoudre le problème posé (structure de données et classes nécessaires à l'implémentation, protocoles de communication)
3. Implémentation de l'architecture choisie
4. Tests fonctionnels de communication entre les différents modules sur le simulateur de conduite
5. Optionnel : Optimiser le temps de communication et d'échanges d'information entre les différents modules

## Contraintes

1. Utiliser Python comme langage pour le modèle et la communication entre les modules
