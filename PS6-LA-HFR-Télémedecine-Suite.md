---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2020/2021
titre: Application Cloud pour la télémedecine
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Omar Abou Khaled
  - Leonardo Angelini
proposé par étudiant: Loris Roubaty
mots-clés: [télémedecine, HFR, cloud computing, microservices]
langue: [F,E]
confidentialité: non
suite: oui
---

## Contexte

Dans le cadre du projet de semestre 5, en collaboration avec l'Hôpital Cantonal de Fribourg, une infrastructure web en microservices à été dévéloppée afin de pouvoir collecter les données des plusieurs dispositifs connectées intégrés dans la valise de télémedecine de l'HFR. Un premier prototype de frontend a été devéloppé afin de tester le fonctionnement de la plateforme cloud. Le but de ce projet est d'implementer une version avancé de l'application web en collaboration avec des utilisateurs cible de la plateforme (qui seront identifiés par l'HFR).


## Objectifs

- Analyse des besoins des utilisateurs cible
- Analyse des librairies et technologies à integrer dans la plateforme
- Conception et implementation de l'application web
- Intégration dans la plateforme des données des appareils intéréssants pour les utilisateurs cible et des données existants de l'HFR
- Tests utilisateur 

## Contraintes
Suite du projet de semestre 5 en collaboration avec l'HFR
- 
