---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2020/2021
titre: JavaScript Perceptual Image Hashing Algorithm
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Omar Abou Khaled
  - Fouad Hannoun
proposé par étudiant: Donzallaz Joé
mots-clés: [Perceptual Hashing, Javascript, NPM]
langue: [F,E]
confidentialité: non
suite: non
---


## Description/Contexte

Ce projet consiste à implémenter une fonction de hashage permettant de calculer l'empreinte numérique d'une image avec un certain degré de précision. Cette implémentation doit être utilisable aussi bien dans un navigateur web que dans un serveur : elle ne devra ainsi dépendre d’aucune librairie externe, être aussi efficace que possible et réduire les chances de collisions au maximum.
L’objectif d’une telle fonction est de pouvoir rapidement identifier si une image est déjà présente dans une base de données et ainsi éviter de la stocker à plusieurs reprises, mais aussi de pouvoir comparer le degré de similarité entre différentes images.
Ref: https://ieeexplore.ieee.org/document/4041692

## Objectifs

Le projet se découpe en 6 étapes :
1.	Analyse du document de recherche « Block Mean Value Based Image Perceptual Hashing »
2. Recherche sur autres technologies dans le domaine 
3. Implémentation de l'algorithme
4. Tests
5. Documentation
