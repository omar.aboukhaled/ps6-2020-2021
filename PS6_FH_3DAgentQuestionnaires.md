---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2020/2021
titre: 3D Virtual assistant handling questionnaires using speech recognition
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
mandants: 
  - Département de psychologie UNIFR - Dr. Marius Rubo
professeurs co-superviseurs:
  - Omar Abou Khaled
  - Fouad Hannoun
mots-clés: [3D character creation, Speech recognition, conversational agents]
langue: [F,E]
confidentialité: non
suite: non
---
\begin{center}
\includegraphics[width=0.6\textwidth]{img/Avatar.png}
\end{center}


## Description/Contexte

Research in human-computer-interaction, psychology and other areas often assesses subjective data from participants which is well-structured. Typical examples are questionnaires where users state their agreement to individual phrases or Yes/No-decision-trees. The easiest, and often most efficient way to assess such data is by handing out written forms (either printed out or online via Google Forms, Unipark etc.) to users. In some cases, however, it may be more practical or more motivating to have a virtual avatar ask each question and record verbal answers using a microphone. This can be the case when data is assessed from children, elderly people or individuals who have difficulties with written language. This project will test possibilities to automatically work through a set of questions based on speech recognition and use a virtual agent to display reactions. 


## Objectifs

1. Explore the latest technologies in the field
2. Design a 3D virtual agent
3. Add voice acting to the agent (synch lips with speech) and speech recognition
4. Build a simple decision tree
5. Test the plateform
