---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2020/2021
titre: Analyse de l'évolution des sentiments sur le Covid au travers des réseaux sociaux
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Omar Abou Khaled
  - Simon Ruffieux
  - Quentin Meteier
mots-clés: Machine Learning, Sentiment Analysis, Covid]
langue: [F,E]
confidentialité: non
suite: non
---

## Contexte

Dans le cadre d'une collaboration avec l'OFSP, nous avons developpé la plateforme Covid Alert Center (https://covidalertcenter.tic.heia-fr.ch/). Cette plateforme a permis de récolter les Tweets émis de comptes suisses et faisant référence au (à la) Covid. Nous avons ainsi pu récolter plusieurs millions de tweets depuis mai 2020. ces tweets sont stockés dans une base de données interne. La plateforme actuelle permet déjà de visualiser différentes statistiques de base sur les tweets collectés.

Dans le cadre de ce projet, nous aimerions cependant aller plus loin en essayant d'identifier les sentiments contenus dans les tweets collectés afin de pouvoir visualiser leur évolution au fil du temps, notamment en relation avec certains événements clés (annonces du conseil fédéral, annonce vaccins, confinement, etc.). L'étudiant devra donc développer les outils permettant de réaliser l'analyse des tweets et de visualiser les résultats. Afin de réaliser le système de détection des sentiments, l'étudiant pourra utiliser le service développé par l'institut IcoSys (https://icosys.ch/new-icoservice-on-board) ou développer ses propres algorithmes. L'étudiant développera ensuite des outils (site web ou interface dynamique) permettant de visualiser les résultats (l'évolution temporelle des sentiments et les événements clés). Une (petite) analyse des résultat obtenus est également attendue.

Ce projet offre l'opportunité de travailler sur des outils innovants (algorithmes d'analyse de sentiments) en se basant sur des données récentes et contrètes. L'étudiant devra faire appel a ses capacités de programmation afin de développer le système d'analyse et de visualisation mais devra également faire appel à ses capacités critiques et "journalistiques" afin d'interpréter les résultats obtenus.


## Objectifs

- Analyse des outils/techniques de détection de sentiments ("Sentiment Analysis")  
- Analyse du système existant (base de données, services icosys)
- Conception et implémentation du pipeline d'analyse de sentiments
- Conception et implémentation d'outils de visualisation (sentiments et événements)
- Interprétation des résultats

## Contraintes

- 
