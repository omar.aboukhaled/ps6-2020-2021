---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2020/2021
titre: Assistant Avatar Bot
filières:
  - Informatique
  - Télécommunications
  - ISC
nombre d'étudiants: 1
mandants: 
  - Swisspro Solutions - Marco De Sousa Ferraz et Carlos Moreira
professeurs co-superviseurs:
  - Omar Abou Khaled
  - Mira El Kamali
  - Karl Daher
mots-clés: [text extraction, chatbot, personal assistant, text to voice]
langue: [F,E]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/bot.png}
\end{center}
```

## Description/Contexte

swisspro Solutions SA est une entreprise suisse de TIC, appartenant au groupe BKW AG qui propose à ses clients des solutions globales de premier ordre dans ce domaine.
Avec quelque 240 spécialistes sur 11 sites en Suisse, nous sommes le fournisseur leader du marché
des solutions de communications unifiées & collaboration. Nos compétences nous permettent en
outre de développer, de mettre en œuvre et d’exploiter des solutions clients intégrées dans les
domaines Networking & Security, services informatiques et informatique du bâtiment.

Un Avatar Bot qui peut vous remplacer lors de vos meetings (vidéo-conférences), lorsqu’il n’est pas possible pour vous d’y participer. Cet Avatar peut aussi vous servir d’assistant, pour vous aider dans vos tâches quotidiennes au bureau.
L’Assistant Avatar Bot pourra prendre position sur des sujets, basé sur les données personnelles du détenteur de l’Avatar, comme l’agenda, les avis personnels sur différents sujets prédéfinis, prendre des rendez-vous ou même accepter des nouvelles tâches à effectuer.

L’Assistant Avatar peut participer à un meeting dans Teams et/ou Zoom et/ou Skype et/ou Rainbow (Alcatel). L’Assistant Avatar enregistre l’ensemble du meeting (Vidéo, Audio, Text), et pourra analyser ces données à posteriori. Une base de connaissances personnalisée, permettra à l’Assistant Avatar Bot de répondre aux questions des participants, et de prendre position sur des sujets prédéfinis.

Il y a 3 phases dans ce projet:
Phase 1
Capter le flux Vidéo/Audio/Texte des outils tels que MS Teams, Zoom, skype ou Rainbow. Déterminer la meilleure solution pour avoir accès aux conférences et leur métha-informations (participants, durée, etc… ), par exemple via une API de Microsoft.
Enregistrer ces flux de données, pour une analyse future.
Phase 2
Analyser le contexte en live ( Audio et Texte ), et déterminer si l’Assistant Avatar Bot doit répondre ou prendre position, selon ce que les participants disent.
L’Assistant Avatar Bot construit sa réponse basée sur des « skills » prédéfinis ; dans le cadre de ce projet nous pourrons déterminer 2-3 « skills », puis dans le futur en ajouter de nouveaux.
Générer la réponse sous forme de texte qui pourra être utilisée en Phase 3
Phase 3
Générer le flux Vidéo / Audio / Text avec la réponse de l’Assistant Avatar Bot (créé en Phase2). La réponse du Bot devra être transformée en voix digitale où en text pour le chat. Dans un premier temps la vidéo pourra être une simple image statique, puis nous pourrons évoluer vers un modèle 3D qui serait animé en fonction du message envoyé.

** Nous allons nous concentrer sur la Phase 1. **

## Objectifs/Tâches
 
- Recherche les APIs des outils de conférence
- Extraction du contenu video, audio et texte
- Retranscrire le contenu Audio en texte
- Mettre une architecture du prototype
- Création du prototype
- Test et validation

